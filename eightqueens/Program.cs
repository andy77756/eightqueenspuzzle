﻿using System;
using System.Collections.Generic;

namespace eightqueens
{
    class Program
    {
        static void Main(string[] args)
        {
            int nqueens = 8;

            var results = solveNQueens(nqueens);
            printListArray(results, nqueens);
        }

        public static List<string[,]> solveNQueens(int n)
        {
            var results = new List<string[,]>();
            string[,] result = new string[n, n];

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    result[i, j] = ".";
                }
            }
            backTrack(results, result, 0, 8);
            return results;
        }
        /// <summary>
        /// 回朔法
        /// </summary>
        /// <param name="results">全部解</param>
        /// <param name="result">單一解</param>
        /// <param name="x">初始值</param>
        /// <param name="nqueens">正方形邊長</param>
        private static void backTrack(List<string[,]> results, string[,] result, int x, int nqueens)
        {
            for (int i = 0; i < nqueens; i++)
            {
                if (isValid(result, x, i, nqueens))
                {
                    result[x, i] = "Q";
                    if (x == nqueens - 1)
                    {
                        addResultToList(results, result, nqueens);
                    }
                    else
                    {
                        backTrack(results, result, x + 1, nqueens);
                    }
                    result[x, i] = ".";
                }
            }
        }
        /// <summary>
        /// 判斷是否合法
        /// </summary>
        /// <param name="result"></param>
        /// <param name="x">row number</param>
        /// <param name="y">colum number</param>
        /// <param name="nqueens">正方形邊長</param>
        /// <returns></returns>
        private static bool isValid(string[,] result, int x, int y, int nqueens)
        {
            for (int i = 0; i < x; ++i)
            {
                if (result[i, y] == "Q")
                {
                    return false;
                }
            }

            for (int i = x - 1, j = y - 1; i >= 0 && j >= 0; --i, --j)
            {
                if (result[i, j] == "Q")
                {
                    return false;
                }
            }

            for (int i = x - 1, j = y + 1; i >= 0 && j < nqueens; --i, ++j)
            {
                if (result[i, j] == "Q")
                {
                    return false;
                }
            }
            return true;
        }
        /// <summary>
        /// 將找到的單一解加到List
        /// </summary>
        /// <param name="results">所有解</param>
        /// <param name="result">當下解</param>
        /// <param name="nqueens">正方形邊長</param>
        private static void addResultToList(List<string[,]> results, string[,] result, int nqueens)
        {
            var temp = new string[nqueens, nqueens];
            for (int i = 0; i < nqueens; i++)
            {
                for (int j = 0; j < nqueens; j++)
                {
                    temp[i, j] = result[i, j];
                }
            }
            results.Add(temp);
        }
        /// <summary>
        /// 印出解
        /// </summary>
        /// <param name="results">所有解</param>
        /// <param name="nqueens">正方形邊長</param>
        private static void printListArray(List<string[,]> results, int nqueens)
        {
            foreach (var item in results)
            {
                for (int i = 0; i < nqueens; i++)
                {
                    for (int j = 0; j < nqueens; j++)
                    {
                        Console.Write(item[i, j]);
                    }
                    Console.WriteLine();
                }
                Console.WriteLine("--------------------");
            }
        }

    }
}
